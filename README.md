# Chip-8 Emulator

Following a tutorial on udemy for building a chip-8 emulator in C using SDL.

# Setup on Ubuntu

```
sudo apt update
sudo apt upgrade
sudo apt-get install build-essential libsdl2-2.0-0 libsdl2-dev libsdl2-image-2.0-0 libsdl2-image-dev

# For cross-compiling 32-bit on 64-bit platform:
sudo apt install lib32z1
sudo apt install gcc-multilib
sudo apt-get install libsdl2-2.0-0:i386 libsdl2-dev:i386 libsdl2-image-2.0-0:i386 libsdl2-image-dev:i386
```

> We have set `-m32` in our Makefile for 32-bit binaries as recommended in the tutorial.

